# coding=utf-8

"""
https://www.rose-hulman.edu/~finn/CCLI/Notes/day09.pdf
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2015"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import logging
log = logging.getLogger('phermite_interpolation')

import numpy as np

base_type = np.float64

class L(object):

    def __init__(self, points):
        self.p = np.array(points)
        self.n = len(points)
        # Denominator factor table
        self.prod = np.array([np.prod([(self.p[i]-self.p[m])
                                       for m in range(self.n) if m != i])
                              for i in range(self.n)])
        self.d_fac = 1.0/self.prod

    # def prod(self, i, j, p):
    #     return np.prod([(p-self.p[i]) for i in range(self.n) if i!=j])

    def val(self, i, p):
        return np.prod([(p-self.p[m])
                        for m in range(self.n) if m != i]) * self.d_fac[i]

    def dval(self, i, p):
        return np.sum([np.prod([p-self.p[l]
                                for l in range(self.n) if l != k and l != i])
                       for k in range(self.n) if k != i]) * self.d_fac[i]
        # By logarithmic derivative, gives NaN at knot points
        # return self.val(j,p) * np.sum([(1.0/(p-self.p[i]))
        #                                for i in range(self.n) if i!=j], 0)


# class H(object):

#     def __init__(self, points, vals, dvals):
#         self.p, self.v, self.dv = points, vals, dvals
#         self.n = len(points)
#         self.L = L(self.p)

#     def h(self, j, p):
#         return (1 - 2 * (p-self.p[j]) *
#                 self.L.dval(j, self.p[j])) * self.L.val(j, p)**2

#     def hh(self, j, p):
#         return (p - self.p[j]) * self.L.val(j, p)**2

#     def val(self, p):
#         return np.sum([self.v[j] * self.h(j, p) +
#                        self.dv[j] * self.hh(j, p) for j in range(self.n)])

#     def vals(self, ps):
#         return [self.val(p) for p in ps]




class HermiteSingleSegment(object):
    """Single segment Hermite interpolator."""

    def __init__(self, t0, t1, f0, f1, df0, df1):
        """Interpolate from (t0, f0, df0) to (t1, f1, df1). 'f's are vectors
        of positions, 'df's are vectors of velocities.
        """
        self.t0, self.t1, self.f0, self.f1, self.df0, self.df1 = t0, t1, f0, f1, df0, df1
        delta_t = t1 - t0
        df_avg = (f1 - f0) / delta_t
        self.a = f0
        self.b = df0
        self.c = (df_avg - df0) / delta_t
        self.d = (df1 + df0 - 2 * df_avg) / delta_t**2

    def val(self, t):
        """Evaluate the trajectory position at time 't'."""
        dt0 = t - self.t0
        dt0sq = dt0**2
        return (self.a + self.b * dt0 + self.c * dt0sq +
                self.d * dt0sq * (t-self.t1))

    def vals(self, ts):
        """Evaluate the trajectory positions for the times given in the
        sequence 'ts'.
        """
        return np.array([self.val(t) for t in ts])


class PWHermite(object):
    """Piece-wise, single segment Hermite interpolator."""
    
    def __init__(self, t,  f, df):
        """A set of times are given in the sequence 't' with corresponding row
        vectors of positions in 'f' and corresponding row vectors of
        velocities in 'df'.
        """
        self.t, self.f, self.df = t, f, df
        self.dim = f.shape[1]  # Dimension
        self.h_segm = [] # Sequence of single segments
        self.n = 0  # Number of segments
        self.t_start = t[0]
        self.f_start = f[0]
        self.df_start = df[0]
        for i in range(1, len(self.t)):
            self.add_segment(self.t[i], self.f[i], self.df[i])

    def get_latest_point(self):
        if len(self.h_segm) == 0:
            return self.t_start, self.f_start, self.df_start
        else: 
            return self.h_segm[-1].t1, self.h_segm[-1].f1, self.h_segm[-1].df1
            
    def add_segment(self, t, f, df):
        """Add a segment at the end of the sequence. The segment is described
        by time 't', position vector 'f', and velocity vector
        'df'. 't' must be later than the time for the latest point in
        the sequence.
        """
        t0, f0, df0 = self.get_latest_point()
        if t < t0:
            raise Exception('New point time must be later ' +
                            'than latest point time.')
        log.debug('Adding segment with times {}, func vals {}, func derivs {}'
                  .format([t0, t], [f0, f], [df0, df]))
        self.h_segm.append(HermiteSingleSegment(t0, t, f0, f, df0, df))
        self.n += 1

    def val(self, t):
        """Evaluate the position at time 't'."""
        if t < self.t_start:
            raise Exception("Time was earlier than earliest.")
        for h in self.h_segm:
            if t <= h.t1:
                return h.val(t)
        raise Exception("Time was later than latest.")

    def vals(self, ts):
        """Evaluate the positions at times 'ts'."""
        return np.array([self.val(t) for t in ts])





class HermiteCubicSingleSegment:
    
    def __init__(self, t0, t1, p0, p1, v0, v1):
        self.t0 = t0
        self.t1 = t1
        self.p0 = p0
        self.p1 = p1
        self.v0 = v0
        self.v1 = v1
        self.dt = dt = t1 - t0
        self.parvec = np.array([p0, dt * v0, dt * v1, p1])

    def H3(self, u):
        u2 = u**2
        u3 = u**3
        return np.array([
            1 - 3 * u2 + 2 * u3,
            u - 2 * u2 + u3,
            -u2 + u3,
            3 * u2 - 2 * u3])

    def val(self, t):
        u = (t - self.t0) / self.dt
        return self.H3(u).dot(self.parvec)

    def vals(self, ts):
        """Evaluate the trajectory positions for the times given in the
        sequence 'ts'.
        """
        return np.array([self.val(t) for t in ts])



class PWCH(object):
    """Piece-wise, single segment cubic Hermite interpolator."""
    
    def __init__(self, ts,  ps, vs):
        """A set of times are given in the sequence 't' with corresponding row
        vectors of positions in 'f' and corresponding row vectors of
        velocities in 'df'.
        """
        self.ts, self.ps, self.vs = ts, ps, vs
        self.dim = ps.shape[1]  # Dimension
        self.h_segm = [] # Sequence of single segments
        self.n = 0  # Number of segments
        self.t_start = ts[0]
        self.p_start = ps[0]
        self.v_start = vs[0]
        for i in range(1, len(self.ts)):
            self.add_segment(self.ts[i], self.ps[i], self.vs[i])

    def get_latest_point(self):
        if len(self.h_segm) == 0:
            return self.t_start, self.p_start, self.v_start
        else: 
            return self.h_segm[-1].t1, self.h_segm[-1].p1, self.h_segm[-1].v1
            
    def add_segment(self, t, p, v):
        """Add a segment at the end of the sequence. The segment is described
        by time 't', position vector 'f', and velocity vector
        'df'. 't' must be later than the time for the latest point in
        the sequence.
        """
        t0, p0, v0 = self.get_latest_point()
        if t < t0:
            raise Exception('New point time must be later ' +
                            'than latest point time.')
        log.debug('Adding segment with times {}, func vals {}, func derivs {}'
                  .format([t0, t], [p0, p], [v0, v]))
        self.h_segm.append(HermiteCubicSingleSegment(t0, t, p0, p, v0, v))
        self.n += 1

    def val(self, t):
        """Evaluate the position at time 't'."""
        if t < self.t_start:
            raise Exception("Time was earlier than earliest.")
        for h in self.h_segm:
            if t <= h.t1:
                return h.val(t)
        raise Exception("Time was later than latest.")

    def vals(self, ts):
        """Evaluate the positions at times 'ts'."""
        return np.array([self.val(t) for t in ts])


    

class HermiteQuinticSingleSegment:

    def __init__(self, t0, t1, p0, p1, v0, v1, a0, a1):
        self.t0 = t0
        self.t1 = t1
        self.p0 = p0
        self.p1 = p1
        self.v0 = v0
        self.v1 = v1
        self.a0 = a0
        self.a1 = a1
        self.dt = dt = t1 - t0
        self.parvec = np.array([p0, dt * v0, dt**2 * a0, dt**2 * a1, dt * v1, p1])

    def H5(self, u):
        u2 = u**2
        u3 = u**3
        u4 = u**4
        u5 = u**5
        return np.array([
            1 - 10 * u3 + 15 * u4 - 6 * u5,
            u  - 6.0 * u3 + 8.0 * u4 - 3.0 * u5,
            0.5 * u2 - 1.5 * u3 + 1.5 * u4 - 0.5 * u5,
            0.5  * u3 - u4 + 0.5 * u5,
            -4.0 * u3 + 7.0 * u4 - 3.0 * u5,
            10.0 * u3 - 15.0 * u4 + 6.0 * u5])

    def val(self, t):
        u = (t - self.t0) / self.dt
        return self.H5(u).dot(self.parvec)

    def vals(self, ts):
        """Evaluate the trajectory positions for the times given in the
        sequence 'ts'.
        """
        return np.array([self.val(t) for t in ts])


class PWQH(object):
    """Piece-wise, single segment quintic Hermite interpolator."""
    
    def __init__(self, ts,  ps, vs, as_):
        """A set of times are given in the sequence 't' with corresponding row
        vectors of positions in 'f' and corresponding row vectors of
        velocities in 'df'.
        """
        self.ts, self.ps, self.vs, self.as_ = ts, ps, vs, as_
        self.dim = ps.shape[1]  # Dimension
        self.h_segm = [] # Sequence of single segments
        self.n = 0  # Number of segments
        self.t_start = ts[0]
        self.p_start = ps[0]
        self.v_start = vs[0]
        self.a_start = as_[0]
        for i in range(1, len(self.ts)):
            self.add_segment(self.ts[i], self.ps[i], self.vs[i], self.as_[i])

    def get_latest_point(self):
        if len(self.h_segm) == 0:
            return self.t_start, self.p_start, self.v_start, self.a_start
        else: 
            return self.h_segm[-1].t1, self.h_segm[-1].p1, self.h_segm[-1].v1, self.h_segm[-1].a1
            
    def add_segment(self, t, p, v, a):
        """Add a segment at the end of the sequence. The segment is described
        by time 't', position vector 'f', and velocity vector
        'df'. 't' must be later than the time for the latest point in
        the sequence.
        """
        t0, p0, v0, a0 = self.get_latest_point()
        if t < t0:
            raise Exception('New point time must be later ' +
                            'than latest point time.')
        log.debug('Adding segment with times {}, func vals {}, func derivs {}'
                  .format([t0, t], [p0, p], [v0, v], [a0, a]))
        self.h_segm.append(HermiteQuinticSingleSegment(t0, t, p0, p, v0, v, a0, a))
        self.n += 1

    def val(self, t):
        """Evaluate the position at time 't'."""
        if t < self.t_start:
            raise Exception("Time was earlier than earliest.")
        for h in self.h_segm:
            if t <= h.t1:
                return h.val(t)
        raise Exception("Time was later than latest.")

    def vals(self, ts):
        """Evaluate the positions at times 'ts'."""
        return np.array([self.val(t) for t in ts])
