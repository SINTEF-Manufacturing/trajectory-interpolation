# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2015"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

try:
    import pyximport
    pyximport.install()
    from .chermite_interpolation import HermiteSingleSegment, PWHermite
except:
    print('Warning : Using pure Python+Numpy implementation for ' +
          'trajectory implementation!')
    from .phermite_interpolation import HermiteSingleSegment, PWHermite
