# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2015"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import logging
log = logging.getLogger('chermite_interpolation')

import numpy as np
cimport numpy as np

base_type = np.float64
ctypedef np.float64_t cbase_type

cdef class L(object):

    cdef np.uint8_t n
    cdef np.ndarray t, prod, d_fac, t_diff

    def __init__(self, np.ndarray[cbase_type] t):
        self.t = t
        self.n = len(t)
        cdef np.uint8_t m
        cdef np.uint8_t i
        # Denominator factor table
        self.prod = np.array([np.prod([(self.t[i]-self.t[m])
                                       for m in range(self.n) if m!=i])
                              for i in range(self.n)],
                             dtype=base_type)
        self.d_fac = 1.0/self.prod


    cpdef cbase_type val(self, np.uint8_t i, cbase_type t):
        cdef np.uint8_t m
        cdef cbase_type prod = 1.0
        for m in range(self.n):
            if m!=i:
                prod *= self.t_diff[m]
        return prod * self.d_fac[i]

    cpdef cbase_type dval(self, np.uint8_t i, cbase_type t):
        """
        http://math.stackexchange.com/questions/809927/first-derivative-of-lagrange-polynomial
        """
        cdef np.uint8_t k
        cdef np.uint8_t l
        cdef cbase_type sum = 0.0
        cdef cbase_type prod
        for k in range(self.n):
            prod = 1.0
            for l in range(self.n):
                if l!=k: # and l!=i: 
                    prod *= self.t_diff[l]
            sum += prod
        return sum * self.d_fac[i]
        # By logarithmic derivative, gives NaN at knot points
        # return self.val(i,t) * np.sum([(1.0/(t-self.t[m])) for m in range(self.n) if m!=i])

cdef class HermiteSingleSegment(object):
    cdef public cbase_type t0, t1
    cdef public np.ndarray f0, f1, df0, df1
    cdef public np.ndarray a, b, c, d
    def __cinit__(self, cbase_type t0, cbase_type t1,
                  np.ndarray[cbase_type] f0,
                  np.ndarray[cbase_type] f1,
                  np.ndarray[cbase_type] df0,
                  np.ndarray[cbase_type] df1):
        self.t0, self.t1, self.f0, self.f1, self.df0, self.df1 = t0, t1, f0, f1, df0, df1
        delta_t = t1 - t0
        df_avg = (f1 - f0) / delta_t
        self.a = f0
        self.b = df0
        self.c = (df_avg - df0) / delta_t
        self.d = (df1 + df0 - 2 * df_avg) / delta_t**2

    cpdef val(self, t):
        cdef cbase_type dt0 = t - self.t0
        cdef cbase_type dt0sq = dt0**2
        return self.a + self.b * dt0 + self.c * dt0sq + self.d * dt0sq * (t-self.t1)

    cpdef np.ndarray[cbase_type] vals(self, np.ndarray[cbase_type] ts):
        return np.array([self.val(t) for t in ts])


# # Largely of no known use!!!
# cdef class HermiteMultiSegment(object):

#     cdef public np.uint8_t n, dim
#     cdef public np.ndarray t, f, df, t_diff
#     cdef L L
    
#     def __cinit__(self, np.ndarray[cbase_type] t,
#                   np.ndarray[cbase_type, ndim=2] f,
#                   np.ndarray[cbase_type, ndim=2] df):
#         self.t, self.f, self.df = t, f, df
#         self.n = f.shape[0]
#         self.dim = f.shape[1]
#         self.t_diff = np.empty(self.n, dtype=base_type)
#         self.L = L(self.t)
        
#     cpdef cbase_type h(self, np.uint8_t i, cbase_type t):
#         return (1.0 - 2.0 * self.t_diff[i] * self.L.dval(i, self.t[i])) * self.L.val(i, t)**2

#     cpdef cbase_type hh(self, np.uint8_t i, cbase_type t):
#         return self.t_diff[i] * self.L.val(i, t)**2

#     cpdef _set_t(self, cbase_type t):
#         cdef np.uint8_t i
#         for i in range(self.n):
#             self.t_diff[i] = t - self.t[i]
#         # Share computation with L
#         self.L.t_diff = self.t_diff
    
#     cpdef np.ndarray[cbase_type] val(self, cbase_type t):
#         self._set_t(t)
#         cdef np.uint8_t i
#         cdef np.ndarray[cbase_type] accum = np.zeros(self.dim, dtype=base_type)
#         for i in range(self.n):
#             accum += self.f[i] * self.h(i, t) + self.df[i] * self.hh(i, t)
#         return accum

#     cpdef np.ndarray[cbase_type] vals(self, np.ndarray[cbase_type] ts):
#         return np.array([self.val(t) for t in ts])
    

cdef class PWHermite(object):

    cdef public np.uint8_t n, dim
    cdef public np.ndarray t, f, df, f_start, df_start
    #cdef np.ndarray h_segm
    cdef public list h_segm
    cdef cbase_type t_start
    
    def __cinit__(self, np.ndarray[cbase_type] t, np.ndarray[cbase_type, ndim=2] f, np.ndarray[cbase_type, ndim=2] df):
        self.t, self.f, self.df = t, f, df
        self.dim = f.shape[1]  # Dimension
        self.h_segm = []
        self.n = 0 # Number of segments
        self.t_start = t[0]
        self.f_start = f[0]
        self.df_start = df[0]
        for i in range(1, len(self.t)):
            self.add_segment(self.t[i], self.f[i], self.df[i])

    cpdef get_latest_point(self):
        if len(self.h_segm) == 0:
            return self.t_start, self.f_start, self.df_start
        else: 
            return self.h_segm[-1].t1, self.h_segm[-1].f1, self.h_segm[-1].df1
            
    cpdef add_segment(self,
                      cbase_type t,
                      np.ndarray[cbase_type] f,
                      np.ndarray[cbase_type] df):
        t0, f0, df0 = self.get_latest_point()
        if t < t0:
            raise Exception("New point time must be later than latest point time.")
        log.debug('Adding segment with times {}, func vals {}, func derivs {}'.format(
            np.array([t0,t], dtype=base_type),
            np.array([f0, f], dtype=base_type),
            np.array([df0, df], dtype=base_type)))
        self.h_segm.append(HermiteSingleSegment(t0, t, f0, f, df0, df))
        self.n += 1
        
    cpdef np.ndarray[cbase_type] val(self, cbase_type t_int):
        cdef HermiteSingleSegment h
        if t_int < self.t_start:
            raise Exception("Time was earlier than earliest.")
        for h in self.h_segm:
            if t_int <= h.t1:
                return h.val(t_int)
        raise Exception("Time was later than latest.")

    cpdef np.ndarray[cbase_type, ndim=2] vals(self, np.ndarray[cbase_type] ts):
        return np.array([self.val(t) for t in ts])
