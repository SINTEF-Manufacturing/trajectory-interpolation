#!/usr/bin/python3
# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2015"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import logging
import pathlib

logging.basicConfig(level='INFO')

import numpy as np
# import pyximport; pyximport.install()
import matplotlib.pyplot as plt

from trajectory_interpolation.phermite_interpolation import HermiteSingleSegment, PWHermite, base_type


plt_folder = pathlib.Path('plots/')
if not plt_folder.exists():
    plt_folder.mkdir()

L_seg = 0.1 
T_seg = 0.1
dt = 2e-3


def draw_hit_corner():
    global t, p, v, interp, period, dt, t_base, N, dim, path_nodes
    d_ramp = 0.33
    t = T_seg * np.array([0, (1-d_ramp), 1, (1+d_ramp), 2], dtype=base_type)
    path_nodes = L_seg * np.array([[0,0], [1,0], [1,1]])
    p = np.array([[0, 0],
                  [(1-d_ramp) * L_seg, 0],
                  [L_seg, 0],
                  [L_seg, d_ramp * L_seg],
                  [L_seg, L_seg]],
                 dtype=base_type)
    spd = L_seg / T_seg
    v = spd * np.array([[1, 0], [1, 0], [0.7, 0.7],
                  [0, 1], [0, 1]],
                 dtype=base_type)
    N, dim = p.shape
    interp = PWHermite(t, p, v)
    plot('hit_corner')


def draw_free_corner():
    global t, p, v, interp, period, dt, t_base, N, dim, path_nodes
    path_nodes = L_seg * np.array([[0,0], [1,0], [1,1]])
    d_blend = 0.33
    t = T_seg * np.array([0, (1-d_blend) , (1+d_blend), 2], dtype=base_type)
    p = L_seg * np.array([[0, 0],
                          [1 - d_blend, 0],
                          [1, d_blend],
                          [1, 1]],
                         dtype=base_type)
    spd = L_seg / T_seg
    v = spd * np.array([[1, 0],
                        [1, 0],
                        [0, 1],
                        [0, 1]],
                       dtype=base_type)
    N, dim = p.shape
    interp = PWHermite(t, p, v)
    plot('free_corner')


def draw_outer_loop():
    global t, p, v, interp, period, dt, t_base, N, dim, spd, L_seg
    path_nodes = L_seg * np.array([[0,0], [1,0], [1,1]])
    d_loop = 1.0
    t = T_seg * np.array([0, 1, 1 + d_loop, 2 + d_loop], dtype=base_type)
    p = L_seg * np.array([[0, 0],
                          [1, 0],
                          [1, 0],
                          [1, 1]],
                         dtype=base_type)
    spd = L_seg / T_seg
    v = spd * np.array([[1, 0],
                        [1, 0],
                        [0, 1],
                        [0, 1]],
                 dtype=base_type)
    N, dim = p.shape
    interp = PWHermite(t, p, v)
    plot('outer_loop')


def draw_stop_corner():
    global t, p, v, interp, period, dt, t_base, N, dim, path_nodes
    path_nodes = L_seg * np.array([[0,0], [1,0], [1,1]])
    d_stop = 0.33
    t = T_seg * np.array([0, 1-d_stop, 1, 1+d_stop, 2], dtype=base_type)
    p = L_seg * np.array([[0, 0],
                          [1-d_stop, 0],
                          [1, 0],
                          [1, d_stop],
                          [1, 1]],
                         dtype=base_type)
    spd = 1.0
    v = spd * np.array([[1, 0],
                        [1, 0],
                        [0, 0],
                        [0, 1],
                        [0, 1]],
                 dtype=base_type)
    N, dim = p.shape
    interp = PWHermite(t, p, v)
    plot('stop_corner')


def draw_face_mill(n_strokes=7, stroke_spacing = 0.01):
    global t, p, v, interp, period, dt, t_base, N, dim, path_nodes
    spd = L_seg / T_seg
    t_via = 0.5 * T_seg  # 0.5 * np.pi * stroke_spacing / spd
    t = np.array([[i*(T_seg + t_via), i*(T_seg + t_via) + T_seg]
                 for i in range(n_strokes)], dtype=base_type).flatten()
    p = []
    v = []
    for i in range(n_strokes):
        p += [[(i % 2) * L_seg, i*stroke_spacing]]
        v += [[(((i + 1) % 2) - 0.5) * 2*spd, 0]]
        p += [[((i + 1) % 2) * L_seg, i * stroke_spacing]]
        v += [[(((i + 1) % 2) - 0.5) * 2 * spd, 0]]

    path_nodes = p = np.array(p, dtype=base_type)
    v = np.array(v, dtype=base_type)
    N, dim = p.shape
    interp = PWHermite(t, p, v)
    plot('face_mill')


def time_test():
    import time
    n_ts = len(t_base)
    intp_vals = np.empty((n_ts, dim))
    t0 = time.time()
    for i in range(n_ts):
        intp_vals[i] = interp.val(t_base[i])
    # interp.vals(t_base)
    # [interp.val(t) for t in t_base]
    print((time.time()-t0)/len(t_base))


marker_size = 1
interleave = 1
vel_scale = 0.01


def plot(file_name_base=None, show=None):
    global int_vals, int_lims, int_ptp, t_base, ax, fig, path_nodes
    fig, ax = plt.subplots()
    # plt.plot(*interp.vals(t_base[::10]).T, color='green',
    #          marker='o', linestyle='',  ms=2)
    t_base = np.arange(t[0], t[-1], dt, dtype=base_type)
    ax.plot(*path_nodes.T, color='red', marker='x',
                linestyle='',  ms=10, label='path node')
    for i in range(N):
        #ax.plot(*p[i], color='red', marker='o',
        #linestyle='',  ms=5)
        if np.linalg.norm(v[i]) > 1e-3:
            v_norm = np.linalg.norm(v)
            ax.arrow(*np.append(p[i], vel_scale*v[i]),
                      width=0.0002*v_norm,
                      head_width=0.001*v_norm,
                      head_length=0.001*v_norm,
                      color='red')
        # plt.axvline(t_i, color='red')
    ax.plot(*p.T, color='red', marker='o',
            linestyle='',  ms=5, label='trajectory node')
    int_vals = interp.vals(t_base)
    int_lims = np.vstack((int_vals.min(axis=0), int_vals.max(axis=0)))
    int_ptp = int_vals.ptp(axis=0)
    ax.set_xlim(int_lims.T[0] + [-0.15*int_ptp[0], 0.15*int_ptp[0]])
    ax.set_ylim(int_lims.T[1] + [-0.15*int_ptp[1], 0.15*int_ptp[1]])
    ax.set_xlabel('$x$ [$m$]')
    ax.set_ylabel('$y$ [$m$]')
    ax.plot(*int_vals[:, :2][::interleave].T, color='green', marker='o',
             linestyle='',  ms=marker_size, label='trajectory')
    # plt.plot(*int_vals[:,2:][::interleave].T, color='blue',
    #          marker='o', linestyle='',  ms=marker_size)
    # plt.grid(True)
    ax.set_aspect('equal', 'datalim')
    ax.legend()
    if file_name_base is not None:
        fig.savefig('plots/' + file_name_base + '_curve.pdf')
    else:
        plt.show()
    num_derivs()
    plt.cla()
    marker = '.'
    fig, axv = plt.subplots()
    axv.plot(t_base[:-1], traj_vel_n,
             color='blue', marker=marker, label='Velocity norm')
    axv.set_xlabel('$t$ [$s$]')
    axv.set_ylabel('$|\mathbf{v}|$ [$m/s$]')
    for tv in axv.get_yticklabels():
        tv.set_color('blue')
    axa = axv.twinx()
    axa.plot(t_base[1:-1], traj_acc_n, color='green',
             marker=marker, label='Acceleration norm')
    axa.set_ylabel('$|\mathbf{a}|$ [$m/s^2$]')
    for ta in axa.get_yticklabels():
        ta.set_color('green')
    fig.legend()
    if file_name_base is not None:
        fig.savefig('plots/' + file_name_base + '_velacc.pdf')
    else:
        plt.show()


def num_derivs():
    global t_base, dt, traj_pos, traj_vel, traj_vel_n, traj_acc, traj_acc_n
    traj_pos = interp.vals(t_base)
    traj_vel = np.diff(traj_pos, axis=0) / dt
    traj_vel_n = np.linalg.norm(traj_vel, axis=1)
    traj_acc = np.diff(traj_vel, axis=0) / dt
    traj_acc_n = np.linalg.norm(traj_acc, axis=1)


draw_face_mill()
draw_stop_corner()
draw_hit_corner()
draw_free_corner()
draw_outer_loop()
#plot()
#num_derivs()
#plt.plot(t_base[:-2], traj_acc_n, marker='.')
#plt.show()
