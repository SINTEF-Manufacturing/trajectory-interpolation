from matplotlib import pyplot as plt
import numpy as np
from sympy import *
init_printing()

var('t, tau, t0, t1, ta, p0, p1, v0, v1, a0, a1')


#subs = ((t0,0.0),(t1,1.0),(p0,0.5),(v0,-1.2),(a0,-1.325),(a1,4.975))
#subs = dict(t0=0.0,t1=1.0,p0=0.5,v0=-1.2,a0=3.25,a1=-2.0)
init_time = 0.0
tgt_time = 0.01
init_p = 0.1
tgt_p = 0.01
init_v = 10.0
tgt_v = 5.0

initsubs = dict(t0=init_time, p0=init_p, v0=init_v)
tgtsubs = dict(t1=tgt_time, p1=tgt_p, v1=tgt_v)
period = tgtsubs[t1.name]-initsubs[t0.name]
times = np.arange(initsubs[t0.name]-0.05*period,tgtsubs[t1.name]+0.05*period, 0.01 * period)



bsubs={}
bsubs.update(initsubs)
bsubs.update(tgtsubs)

tm=(t1+t0)/2
T = t1-t0

def chi(s, a, b):
    H=Heaviside
    return H(s-a)*H(b-s)

def rampup(s,a,b):
    return chi(s,a,b) * (s-a)/(b-a) # + Heaviside(s-b)

def rampdn(s,a,b):
    return chi(s,a,b) * (b-s)/(b-a) # + Heaviside(b-s)

#def circ_bump(s, a, b):

# def l(s,ss,j):
#     return prod([(s-sk)/(ss[j]-sk) for sk in ss if not ss.index(sk)==j])

# def h(s,ss,j):
#     return (1-2*(s-ss[j])*diff(l(s,ss,j),s))*l(s,ss,j)**2

# def hh(s,ss,j):
#     return (s-ss[j])*l(s,ss,j)**2

# def H(s,ss, fs, dfs):
#     return sum([fs[j]*h(s,ss,j) + dfs[j]*hh(s,ss,j) for j in range(len(ss))])

#var('f0,f1,df0,df1')

import hermite_interpolation
 
p = hermite_interpolation.H([t0,t1], [p0,p1], [v0,v1])(t)

# p_direct = (
#     (1+2*(t-t0)/(t1-t0))*((t1-t)/(t1-t0))**2*p0
#     + (t-t0)*((t1-t)/(t1-t0))**2*v0
#     + (1+2*(t1-t)/(t1-t0))*((t0-t)/(t0-t1))**2*p1
#     + (t-t1)*((t0-t)/(t0-t1))**2*v1
#     )
    
v = diff(p,t)
a = diff(p,t,2)
# plot(p.subs(bsubs), v.subs(bsubs), a.subs(bsubs)
#      ,(t,-0.01,0.11))
#v = integrate(a)
#v = v0 + chi(t0,tm) * a0*(t-t0) + chi(tm,t1) (a0 * T/2 + a1 * (t-tm)) + chi() 
#v = v0 + Piecewise((0, t<t0),(a0*(t-t0), t<tm), (a0*(tm-t0)+a1*(t-tm), t<t1), (a0*(tm-t0)+a1*(t1-tm), t>=t1))
#v_conc = np.vectorize(lambdify(t,v.subs(subs)))
#p = p0+integrate(v.subs(t,tau), (tau, t0, t), conds='separate')
#p_conc = np.vectorize(lambda x:p.subs(subs).subs(t,x).doit())

# plt.plot(times, p_conc(times))
# plt.plot(times, v_conc(times))
# plt.show()

# From manual calculus
# pt1 = p0 + v0*T + 3/8*a0*T**2 + 1/8*a1*T**2
# vt1 = v0 + a0*T/2 + a1*T/2
# =>
#PVT1 = np.array([[3*T**2/8, T**2/8], [T/2, T/2]])
#rhs = np.array([p1-p0-v0*T, v1-v0])
# a = np.linalg.inv(PVT1).dot(lhs)

#vt1=v.subs(bsubs).doit().subs(t,1)
#pt1=p.subs(bsubs).doit().subs(t,1)

# def compute_accels(): #tm0, pos0, vel0, tm1, pos1, vel1):
#     # bsubs = dict(t0=tm0,p0=pos0,v0=vel0, t1=tm1, t=tm1)
#     # pt1=p.subs(t,tm1).subs(bsubs).doit()
#     # vt1=v.subs(t,tm1).subs(bsubs).doit()
#     a = solve([(p-p1).subs(t,t1).subs(bsubs).doit(),
#                (v-v1).subs(t,t1).subs(bsubs).doit()],
#               [a0,a1])
#     return a

#asubs=compute_accels()
#a=solve([vt1-v1,pt1-p1],[a0,a1])
#asubs = dict(a0=a[a0].subs(bsubs),a1=a[a1].subs(bsubs))
#pt=p.subs(asubs).subs(bsubs)
#vt=v.subs(asubs).subs(bsubs)
cp=p.subs(bsubs)
cv=v.subs(bsubs)
vcp = np.vectorize(lambda s: cp.subs(t,s))
vcv = np.vectorize(lambda s: cv.subs(t,s))

print(vcp(init_time), vcv(init_time))
print(vcp(tgt_time), vcv(tgt_time))


plt.plot(times, vcp(times), color='g')
plt.plot(times, vcv(times), color='b')
plt.axvline(tgtsubs[t1.name], color='red')
plt.axvline(initsubs[t0.name], color='red')
plt.grid(True)
plt.show()

